@extends('layouts.app')

@section('title', ' Просмотр объявления')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if (!Auth::guest())
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>{{$ad->title}}</h4>
                            <span style="font-size: 12px;color: #a3a3a3;">by {{$ad->author}}</span>
                            <span style="font-size: 12px;color: #a3a3a3;">crated: {{$ad->created_at}}</span>
                        </div>
                        <div class="panel-body">
                            <p>{{$ad->description}}</p>
                        </div>

                        @if($ad['author'] == Auth::user()->username)
                            <div class="panel-footer">
                                <a  class="btn btn-danger" href="{{route('delete', ['id' => $ad->id])}}"> delete</a>
                                <a  class="btn btn-warning" href="{{'/edit/'.$ad['id']}}">edit</a>
                            </div>
                        @endif
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection