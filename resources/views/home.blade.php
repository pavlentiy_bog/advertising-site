@extends('layouts.app')

@section('title', ' Лучшие объявления')

@section('content')

    <div class="container">

        @if (session('success'))
            <div class="row">
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    {{ session('success') }}
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @foreach($adverts as $ad)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{'/'.$ad['id']}}"><h4>{{$ad['title']}}</h4></a>
                            <span style="font-size: 12px;color: #a3a3a3;">by {{$ad['author']}}</span>
                            <span style="font-size: 12px;color: #a3a3a3;">crated: {{$ad['created_at']}}</span>
                        </div>
                        <div class="panel-body">
                            <p>{{$ad['description']}}</p>
                        </div>

                        @if($ad['author'] == Auth::user()->username)
                            <div class="panel-footer">
                                <a href="{{route('delete', ['id' => $ad['id']])}}"> delete</a>
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                {{ $adverts->links() }}
            </div>
        </div>
    </div>
@endsection
