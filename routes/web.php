<?php


use App\Http\Controllers\AdvertController;

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/edit', function () {

        return view('layouts.edit');
    })->name('edit');

    Route::post('/saveNewAd', 'AdvertController@saveNewAd')->name('saveNewAd');
    Route::post('/updateAd', 'AdvertController@updateAd')->name('updateAd');

    Route::get('delete/{id}', function ($id) {

        $response = AdvertController::delite($id);

        return redirect()->route('home')->with('success', 'Объявление успешно удалено');

    })->name('delete');

    Route::get('/{id}', function ($id) {
        $ad = (new \App\Advert)->find($id);
        return view('layouts.one_ad', compact('ad'));
    });

    Route::get('/edit/{id}', function ($id) {
        $ad = (new \App\Advert)->find($id);
        return view('layouts.update_ad', compact('ad'));
    });
});
