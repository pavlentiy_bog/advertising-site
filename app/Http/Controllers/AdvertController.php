<?php

namespace App\Http\Controllers;

use App\Advert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class AdvertController extends Controller
{
    public function saveNewAd(Request $request)
    {
        $this->validator($request->all())->validate();

        $new_advert = (new \App\Advert)->create([
            'title' => $request->title,
            'description' => $request->description,
            'author' => Auth::user()->username,
        ]);
        return redirect('/' . $new_advert->id);
    }

    public function updateAd(Request $request)
    {
        $this->validator($request->all())->validate();
        $update_ad = (new \App\Advert)->find($request->id);
        $update_ad->title = $request->title;
        $update_ad->description = $request->description;
        $update_ad->save();

        return redirect()->back()->with('status', 'Объявление обновлено!');
    }

    public static function delite($id)
    {
        $status = (new \App\Advert)->find($id)->delete();

        return $status;
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|string|max:255',
            'description' => 'required|string||max:255',

        ]);
    }
}
